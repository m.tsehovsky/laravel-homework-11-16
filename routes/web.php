<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::redirect('/', 'customer/order');

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return redirect()->route('customer_order');
});

Route::group([
    'namespace' => 'Admin',
    'prefix' => 'admin',
], function () {
    Route::get('', 'AdminController@showAuthenticate')->name('show_auth');
    Route::post('authenticate', 'AdminController@authenticate')->name('admin_authenticate');
    Route::get('logout', 'AdminController@adminLogout')->name('admin_logout');

});

Route::group([
    'prefix'=>'category',
    'as' => 'category',
], function () {
    Route::get('', 'CategoryController@index')->name('');
    Route::post('create', 'CategoryController@categoryCreate')->name('_create');
    Route::get('restore/all', 'CategoryController@categoryRestoreAll')->name('_restore_all');
    Route::get('delete/{id}', 'CategoryController@categoryDelete')->name('_delete');
    Route::get('restore/{id}', 'CategoryController@categoryRestore')->name('_restore');

});

Route::group([
    'prefix'=>'product',
    'as' => 'product',
], function () {
    Route::get('', 'ProductController@index')->name('');
    Route::post('create', 'ProductController@productCreate')->name('_create');
    Route::get('restore/all', 'ProductController@ProductRestoreAll')->name('_restore_all');
    Route::get('delete/{id}', 'ProductController@ProductDelete')->name('_delete');
    Route::get('restore/{id}', 'ProductController@ProductRestore')->name('_restore');

});

Route::group([
    'prefix'=>'customer',
    'as' => 'customer_',
], function () {
    Route::get('order', 'OrderController@customerIndex')->name('order');
    Route::post('order/create', 'OrderController@customerOrderCreate')->name('order_create');
    Route::get('order/restore/all', 'OrderController@customerOrderRestoreAll')->name('order_restore_all');
    Route::get('order/delete/{id}', 'OrderController@customerOrderDelete')->name('order_delete');
    Route::get('order/restore/{id}', 'OrderController@customerOrderRestore')->name('order_restore');
    Route::post('logout', 'CustomersController@customerLogout')->name('logout');

});

Route::get('admin/order', 'OrderController@adminIndex')->name('admin_order');
