<?php

namespace App\Providers;

use App\Events\CategoryRestoringAll;
use App\Events\ProductDeleting;
use App\Events\ProductRestoring;
use App\Events\ProductRestoringAll;
use App\Listeners\CategoryRestoreAll;
use App\Listeners\ProductDelete;
use App\Listeners\ProductDeleteLog;
use App\Listeners\ProductRestore;
use App\Listeners\ProductRestoreAll;
use App\Listeners\ProductRestoreAllLog;
use App\Listeners\ProductRestoreLog;
use App\Listeners\SendMail;
use App\Models\Category;
use App\Observers\CategoryObserver;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

use App\Events\Logger;
use App\Listeners\SendLog;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],

        Logger::class => [
            SendLog::class,
            SendMail::class,
        ],

        ProductDeleting::class => [
            ProductDelete::class,
            ProductDeleteLog::class,
        ],

        ProductRestoring::class => [
            ProductRestore::class,
            ProductRestoreLog::class,
        ],

        ProductRestoringAll::class => [
            ProductRestoreAll::class,
            ProductRestoreAllLog::class,
        ],

        CategoryRestoringAll::class => [
            CategoryRestoreAll::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        Category::observe(CategoryObserver::class);
    }
}
