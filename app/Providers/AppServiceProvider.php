<?php

namespace App\Providers;

use App\Models\User;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);


        Blade::directive('user', function ($expression) {

            eval("\$myarray = [$expression];");
            if (is_int($myarray[0])) {
                if ((count($myarray) == 2) && (($myarray[1] == 'name') || ($myarray[1] == 'email'))) {
                    return "<?php echo '$myarray[1]' . ': ' . App\Models\User::where('id', $myarray[0])->value('$myarray[1]'); ?>";
                } elseif (count($myarray) == 1) {
                    return "<?php echo 'Name: ' . App\Models\User::where('id', $myarray[0])->value('name') . ', Email: ' . App\Models\User::where('id', $myarray[0])->value('email');
                    ?>";
                }
            }
        });
    }
}
