<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ProductRestoring extends Mailable
{
    use Queueable, SerializesModels;
    public $productId;
    public $name;

    /**
     * Create a new message instance.
     *
     * @param $productId
     * @param $name
     */
    public function __construct($productId, $name)
    {
        $this->productId = $productId;
        $this->name = $name;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail.product-restoring')->subject('Super Store');
    }
}
