<?php

namespace App\Mail;

use App\Listeners\ProductRestoreAll;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use function Livewire\str;

class ProductRestoringAll extends Mailable
{
    use Queueable, SerializesModels;
    public $entity;


    public function __construct($entity)
    {
        $this->entity = $entity;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail.product-restoring-all')->subject('Super Store');
    }
}
