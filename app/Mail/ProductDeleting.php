<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ProductDeleting extends Mailable
{
    use Queueable, SerializesModels;
    public $productId;
    public $name;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($productId, $name)
    {
        $this->productId = $productId;
        $this->name = $name;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail.product-deleting')->subject('Super Store');
    }
}
