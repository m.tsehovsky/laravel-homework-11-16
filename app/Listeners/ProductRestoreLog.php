<?php

namespace App\Listeners;


use App\Events\ProductRestoring;
use App\Models\Product;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Log;

class ProductRestoreLog
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(ProductRestoring $event)
    {
        Log::channel('product_restored_log')->debug('Restored product: ' . Product::withTrashed()->findOrFail($event->productId)->product);
    }
}
