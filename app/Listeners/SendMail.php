<?php

namespace App\Listeners;

use App\Events\Logger;
use App\Mail\OrdersMailer;
use App\Models\Order;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;

class SendMail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(Logger $event)
    {
        Mail::to($event->data->email)->send(new OrdersMailer(Order::where('id', $event->data->id)->get()));
    }
}
