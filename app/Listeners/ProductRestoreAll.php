<?php

namespace App\Listeners;

use App\Events\ProductRestoringAll;
use App\Models\Product;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;
use function Sodium\add;

class ProductRestoreAll
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(ProductRestoringAll $event)
    {
        $data = [];
        foreach ($event->trashedProducts as $product) {
            $users = Product::withTrashed()->where('product' , $product->product)->first()->orders()->get(['email', 'name']);
            $users = $users->unique('email');

            foreach ($users as $user) {
                $data[$user->name . '-' . $user->email][] = $product->product;
            }
        }

        foreach ($data as $key=>$datum) {
            $address = strstr($key, '-');
            $address = substr($address, 1);

            $entity['name'] = strstr($key, '-', true);
            $entity['message'] = $data[$key];

            Mail::to($address)->send(new \App\Mail\ProductRestoringAll($entity));
        }
    }
}
