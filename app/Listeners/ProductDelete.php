<?php

namespace App\Listeners;

use App\Events\ProductDeleting;
use App\Models\Product;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class ProductDelete
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(ProductDeleting $event)
    {
        $users = Product::findOrFail($event->productId)->orders()->get(['name', 'email']);
        $users = $users->unique('email');

        foreach ($users as $user) {
            Mail::to($user->email)->send(new \App\Mail\ProductDeleting($event->productId, $user->name));
        }


    }
}
