<?php

namespace App\Listeners;

use App\Events\ProductRestoring;
use App\Models\Product;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;

class ProductRestore
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(ProductRestoring $event)
    {
        $users = Product::withTrashed()->findOrFail($event->productId)->orders()->get(['email', 'name']);
        $users = $users->unique('email');

        foreach ($users as $user) {
            Mail::to($user->email)->send(new \App\Mail\ProductRestoring($event->productId, $user->name));
        }



    }
}
