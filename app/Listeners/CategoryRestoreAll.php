<?php

namespace App\Listeners;

use App\Events\CategoryRestoringAll;
use App\Mail\CategoryDeleting;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class CategoryRestoreAll
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param CategoryRestoringAll $event
     * @return void
     */
    public function handle(CategoryRestoringAll $event)
    {
        Mail::to(Auth::user()->email)->send(new \App\Mail\CategoryRestoreAll($event->trashedCategories));
    }
}
