<?php

namespace App\Listeners;

use App\Events\ProductDeleting;
use App\Events\ProductRestoring;
use App\Models\Product;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Log;

class ProductDeleteLog
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(ProductDeleting $event)
    {
        Log::channel('product_deleted_log')->debug('Deleted product: '. Product::findOrFail($event->productId)->product);
    }
}
