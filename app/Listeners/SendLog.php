<?php

namespace App\Listeners;

use App\Events\Logger;
use App\Mail\OrdersMailer;
use App\Models\Order;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Log;

class SendLog
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param Logger $event
     * @return void
     * @throws \ReflectionException
     */
    public function handle(Logger $event)
    {
        Log::channel('order_mailer')->debug((new OrdersMailer(Order::where('id', $event->data->id)->get()))->render());
    }
}
