<?php

namespace App\Listeners;

use App\Events\ProductRestoringAll;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Log;

class ProductRestoreAllLog
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(ProductRestoringAll $event)
    {
        $message = '' . PHP_EOL;
        foreach ($event->trashedProducts as $item) {
            $message .= $item->product . PHP_EOL;
        }
        Log::channel('product_restored_log')->debug('Restored these products:' . $message);
    }
}
