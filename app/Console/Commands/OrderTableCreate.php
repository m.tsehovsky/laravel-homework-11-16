<?php

namespace App\Console\Commands;

use App\Models\Order;
use App\Models\Product;
use Illuminate\Console\Command;

class OrderTableCreate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'order:table';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Orders table generation';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $headers = ['Customer\'s name', 'email', 'phone', 'product', 'created_at'];
        $orders = Order::all('name', 'email', 'phone', 'product_id', 'created_at');


        foreach ($orders as $order) {
            $product = Product::findOrFail($order->product_id)->product;
            $order->product_id = $product;

        }

        $this->table($headers, $orders->toArray());
    }
}
