<?php

namespace App\Console\Commands;

use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserGenerator extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:generate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create user interactive';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
//        if ($this->confirm('Are you admin?')) {
//            if (Auth::check()) {
//                $this->line('You are already authorised');
//                $this->line(Auth::user()->name);
//            } else {
//                $this->line('Make authorisation please!');
//                $this->line(Auth::check());
//            }
//        }

        $name = $this->ask("Put user\'s name");
        $email = $this->ask("Put email");
        $password = $this->ask("Put password");
        User::create([
            'name' => $name,
            'email' => $email,
            'password' => Hash::make($password),
        ]);
    }
}
