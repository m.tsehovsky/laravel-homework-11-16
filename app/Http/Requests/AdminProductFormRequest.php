<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AdminProductFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'product' => 'required|string|unique:products,product'
        ];
    }

    public function messages()
    {
        return [
            'required' => 'The :attribute field must be filled',
            'string' => 'The :attribute field must contains only alpha-decimal characters',
            'unique' => 'We already have :attribute with this name',
        ];
    }
}
