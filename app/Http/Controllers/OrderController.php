<?php

namespace App\Http\Controllers;

use App\Events\Logger;
use App\Http\Requests\CustomerOrderFormRequest;
use App\Jobs\MailJob;
use App\Mail\OrdersMailer;
use App\Models\Category;
use App\Models\Order;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class OrderController extends Controller
{
    public function adminIndex()
    {
        $orders = Order::paginate(3);

        return view('admin.admin_order', compact('orders'));
    }

    public function customerIndex(Request $request)
    {
        if($request->has('categoryId')) {
            $categories = Cache::get('categories')->where('id', $request->categoryId)->get();
        } else {
            $categories = Cache::get('categories');
        }

        if (Auth::check()) {
            $orders = Order::where('name', Auth::user()->name)->get();
            $trashedOrders = Order::onlyTrashed()->where('name', Auth::user()->name)->get();

            return view('customers-page', compact('categories', 'orders', 'trashedOrders'));
        }
        return view('customers-page', compact('categories'));
    }

    public function customerOrderCreate(Request $request)
    {
        $data = Order::create(
            [
                'name' => Auth::user()->name,
                'email' => Auth::user()->email,
                'phone' => $request->phone,
                'product_id' => $request->productId,
            ]);

        /*
         * Be careful - second two operation use "queue"
         * Use CLI commands:
         *  php artisan queue:work --stop-when-empty --queue=emails
         *  php artisan queue:work --stop-when-empty --queue=logs
         * */


//        Mail::to($data->email)->queue((new OrdersMailer(Order::where('id', $data->id)->get()))->onQueue('emails'));

//        MailJob::dispatch()->onQueue('logs');
//////        Log::channel('order_mailer')->debug((new OrdersMailer(Order::where('id', $data->id)->get()))->render());

        event(new Logger($data));

        return redirect()->route('customer_order');
    }

    public function customerOrderDelete($orderId)
    {
        Order::findOrFail($orderId)->delete();

        return redirect()->route('customer_order');
    }

    public function customerOrderRestore($orderId)
    {
        Order::onlyTrashed()->findOrFail($orderId)->restore();

        return redirect()->route('customer_order');
    }

    public function customerOrderRestoreAll()
    {
        Order::onlyTrashed()->restore();

        return redirect()->route('customer_order');
    }


}
