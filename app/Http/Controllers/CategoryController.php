<?php

namespace App\Http\Controllers;

use App\Events\CategoryRestoringAll;
use App\Http\Requests\AdminCategoryFormRequest;
use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function index()
    {
        $categories = Category::get();
        $trashedCategories = Category::onlyTrashed()->get();

       return view('admin.categories', compact('categories', 'trashedCategories'));
    }

    public function categoryCreate(AdminCategoryFormRequest $request)
    {
        $result = $request->only('category');
        Category::create(['name' => $result['category']]);
        return response()->redirectTo(route('category'));
    }

    public function categoryDelete($category_id)
    {
        Category::findOrFail($category_id)->delete();
        return response()->redirectTo(route('category'));
    }

    public function categoryRestore($category_id)
    {
        Category::onlyTrashed()->findOrFail($category_id)->restore();
        return response()->redirectTo(route('category'));
    }

    public function categoryRestoreAll()
    {
        $trashedCategories = Category::onlyTrashed()->get();

        event(new CategoryRestoringAll($trashedCategories));

        Category::onlyTrashed()->restore();
        return response()->redirectTo(route('category'));
    }
}
