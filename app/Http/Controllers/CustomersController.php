<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CustomersController extends Controller
{

    public function customerLogout()
    {
        Auth::logout();
        return redirect()->route('customer_order');
    }
}
