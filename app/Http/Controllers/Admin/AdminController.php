<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\AdminAuthenticateFormRequest;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{

    public function showAuthenticate()
    {
        if (Auth::check()) {
            if (((Auth::user()->name === 'admin') || (Auth::user()->email === 'tsehovsky@rambler.ru')) &&
                (Auth::user()->password === '$2y$10$GW1X.uaoCPIB0nkyQPPcBef39o0AtGeV9axe8iydXCIozBMZN/fQa')) {
                return view('admin.admin_page');
            } else {
                return redirect()->route('customer_order');
            }
        } else {
            return view('admin.authentication');
        }
    }

    public function authenticate(AdminAuthenticateFormRequest $request)
    {

        if (!Auth::check()) {
            Auth::attempt($request->only(['name', 'password']));
        }

        return response()->redirectTo(route('show_auth'));
    }

    public function adminLogout()
    {
        Auth::logout();
        return redirect()->route('show_auth');
    }


}
