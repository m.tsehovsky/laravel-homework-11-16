<?php

namespace App\Http\Controllers;

use App\Events\ProductDeleting;
use App\Events\ProductRestoring;
use App\Events\ProductRestoringAll;
use App\Http\Requests\AdminProductFormRequest;
use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class ProductController extends Controller
{
    public function index()
    {
        $categories = Category::get();
        $trashedProducts = Product::onlyTrashed()->get();

        return view('admin.products', compact('categories', 'trashedProducts'));
    }

    public function productCreate(AdminProductFormRequest $request)
    {
        Product::create([
            'product'       => $request->get('product'),
            'category_id'   => $request->get('category'),
        ]);

        return response()->redirectTo(route('product'));
    }

    public function productDelete($productId)
    {
        $message = 'Product with id=' . $productId . ' (' . Product::where('id', $productId)->value('product') . ') was deleted by: ' . Auth::user()->name;

        event(new ProductDeleting($productId));
//        Log::channel('product_deleted_log')->debug($message);

        Product::findOrFail($productId)->delete();

        return redirect()->route('product');
    }

    public function ProductRestore($productId)
    {
        event(new ProductRestoring($productId));

        Product::onlyTrashed()->findOrFail($productId)->restore();

        return redirect()->route('product');
    }

    public function ProductRestoreAll()
    {
        $trashedProducts = Product::onlyTrashed()->get('product');

        event(new ProductRestoringAll($trashedProducts));

        Product::onlyTrashed()->restore();

        return redirect()->route('product');
    }
}
