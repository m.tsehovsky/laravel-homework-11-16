<h1>Dear {{ \Illuminate\Support\Facades\Auth::user()->name }}</h1>

<p>You are restored these categories:</p>
<p>
    <ul>
    @foreach($data as $entity)
        <li>
            Categories ID: {{ $entity->id }}, Category name: "{{ $entity->name }}", Time of restoring: {{ now()->format('d-m-Y H:i:s') }}
        </li>
    @endforeach
</ul>
</p>

