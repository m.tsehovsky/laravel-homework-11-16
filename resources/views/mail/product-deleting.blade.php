Dear {{ $name }}!

We are sorry, but we don't have this product now: <br>
{{ \App\Models\Product::findOrFail($productId)->product }}
