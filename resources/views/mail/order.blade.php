<h1>Hi, dear {{ $order[0]->name }}!</h1>
<br/>
<h2>We are welcome you on our site! Thank you for purchase!</h2>
<p>Your product is: {{ \App\Models\Product::where('id', $order[0]->product_id)->value('product') }}</p>
<p>You purchased its at: {{ $order[0]->created_at->format('d/m/Y H:i:s') }}
