Dear {{ $name }}!

The next product is available now: <br>
{{ \App\Models\Product::withTrashed()->findOrFail($productId)->product }}
