<h1>Dear {{ \Illuminate\Support\Facades\Auth::user()->name }}</h1>

    @if($category->status == 'delete')
        <p>You are deleted the category #{{ $category->id }} with name: "{{ $category->name }}"</p>
        <p>Time of deleting: {{ $category->deleted_at->format('d-m-Y H:i:s') }}</p>
    @elseif($category->status == 'restore')
        <p>You are restored the category #{{ $category->id }} with name: "{{ $category->name }}"</p>
        <p>Time of restoring: {{ now()->format('d-m-Y H:i:s') }}</p>
    @endif

