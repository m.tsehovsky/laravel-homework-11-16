@extends('admin.master')
@section('title', 'Admin page')

@section('content')
    <h1>Hi {{ \Illuminate\Support\Facades\Auth::user()->name }}!</h1>
@endsection
