@extends('admin.master')
@section('title', 'Admin Category page')

@section('content')

    <p>
    <div>
        <h1>Hi {{ \Illuminate\Support\Facades\Auth::user()->name }}! It is the Categories page </h1>
    </div>
    </p>
    <div>
        <p>We have these categories now:</p>
        <table>
            <tr>
                <th>Category name</th>
                <th>Action</th>
            </tr>

            @foreach($categories as $category)
                <tr>
                    <td>{{ $category->name }}</td>
                    <td><a href="{{ route('category_delete', $category->id) }}">Delete</a></td>
                </tr>
            @endforeach
        </table>

    </div>

    <div>
        <p>Create a new category</p>
        <form action="{{ route('category_create') }}" method="post">
            @csrf
            <label for="category">Enter a new category</label>
            <input type="text" name="category" value="{{ old('category') }}">
            <input type="submit" value="Create Category">
        </form>
    </div>

    @include('errors-check')

    @if($trashedCategories->count() > 0)
        <div>
            <p>We have trashed categories now:</p>
            <table>
                <tr>
                    <th>Category name</th>
                    <th>Action</th>
                </tr>

                @foreach($trashedCategories as $category)
                    <tr>
                        <td>{{ $category->name }}</td>
                        <td><a href="{{ route('category_restore', $category->id) }}">Restore</a></td>
                    </tr>
                @endforeach
            </table>

            <br>
            <p>
                <a href="{{ route('category_restore_all') }}">Restore All Categories</a>
            </p>
        </div>
    @endif

@endsection
