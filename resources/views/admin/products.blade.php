@extends('admin.master')
@section('title', 'Admin Product page')

@section('content')

    <p>
    <div>
        <h1>Hi {{ \Illuminate\Support\Facades\Auth::user()->name }}! It is the Products page </h1>
    </div>
    </p>
    <div>
        <p>We have these products now:</p>
        <table>
            <tr>
                <th>Product's name</th>
                <th>Category</th>
                <th>Action</th>
            </tr>

            @foreach($categories as $category)
                @foreach($category->products as $product)
                <tr>
                    <td>{{ $product->product }}</td>
                    <td>{{ $category->name }}</td>

                    <td><a href="{{ route('product_delete', $product->id) }}">Delete</a></td>
                </tr>
                @endforeach
            @endforeach
        </table>

    </div>

    <div>
        <p>Create a new product</p>
        <form action="{{ route('product_create') }}" method="post">
            @csrf
            <label for="">Enter a new product</label>
            <input type="text" name="product" border="1px" value="{{ old('product') }}">
            <label for="category">Select the category</label>
            <select name="category">
                @foreach($categories as $category)
                    <option value="{{ $category->id }}">{{ $category->name }}</option>
                @endforeach
            </select>
            <input type="submit" value="Create Product">
        </form>
    </div>

    @include('errors-check')

    @if($trashedProducts->count() > 0)
        <div>
            <p>We have trashed Products now:</p>
            <table>
                <tr>
                    <th>Product name</th>
                    <th>Action</th>
                </tr>

                @foreach($trashedProducts as $product)
                    <tr>
                        <td>{{ $product->product }}</td>
                        <td><a href="{{ route('product_restore', $product->id) }}">Restore</a></td>
                    </tr>
                @endforeach
            </table>

            <br>
            <p>
                <a href="{{ route('product_restore_all') }}">Restore All Products</a>
            </p>
        </div>
    @endif




@endsection
