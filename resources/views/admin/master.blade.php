<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">

    <title>@yield('title')</title>

</head>
<body>
<div>
@if(\Illuminate\Support\Facades\Auth::check())
    <div>
        @include('admin.menu')
    </div>


@endif

@yield('content')
</div>

</body>

<style>
    form, p, h1 {text-align: center}
    table, td, th {border-collapse: collapse; border: 1px solid gray; margin-left:auto;margin-right:auto}
</style>
</html>
