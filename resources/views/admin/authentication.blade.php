@extends('admin.master')
@section('title', 'Admin authenticate')

@section('content')
    <p>
    <div>
        <h1>It is the admin authenticate page</h1>
    </div>

    </p>

    <br />



    <div>
        <form action="{{ route('admin_authenticate') }}" method="post">
            @csrf
            <label for="email">Put your login please</label>
            <input type="text" name="name" value="{{ old('name') }}">
            <label for="password">Put your password please</label>
            <input type="password" name="password" value="{{ old('password') }}">
            <button type="submit">Authenticate</button>

        </form>
        @include('errors-check')
    </div>
@endsection
