@extends('admin.master')
@section('title', 'Admin Category page')

@section('content')
    <h1>Hello {{ \Illuminate\Support\Facades\Auth::user()->name }}! It is The Page with Customer's Orders.</h1>
    <br>

    @if($orders->count() >0)
    <div>
        <table>
            <tr>
                <th>Customer's name</th>
                <th>Customer's email</th>
                <th>Product name</th>
                <th>Order creared at</th>
            </tr>
            @foreach($orders as $order)
                <tr>
                    <td>{{ $order->name }}</td>
                    <td>{{ $order->email }}</td>
                    <td>{{ $order->products->product }}</td>
                    <td>{{ $order->created_at->format('d-m-Y H:i:s') }}</td>
                </tr>
            @endforeach
        </table>
        {{ $orders->links('vendor.pagination.simple-tailwind') }}
    </div>
    @else
        <p>We don't have orders yet!</p>
    @endif
@endsection
