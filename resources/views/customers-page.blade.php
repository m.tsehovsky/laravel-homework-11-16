@extends('master')
@section('title', 'Customers page')

@section('content')
    <br>
    <br>
    <br>
    <div>
        <form action="{{ route('customer_logout') }}" method="post">
            @csrf
            <button type="submit" style="float: right; display: {{ \Illuminate\Support\Facades\Auth::check() ? 'inline' : 'none' }}">Logout</button>
        </form>
    </div>


    @if(\Illuminate\Support\Facades\Auth::check())
        @user(\Illuminate\Support\Facades\Auth::user()->id)
    @endif

    <p>
        <div>
    {{--        <h1>It is the customers page <?php if (\Illuminate\Support\Facades\Auth::check()) {echo ('for ' .  \Illuminate\Support\Facades\Auth::user()->name);} ?></h1>--}}
            <h1>It is the customers page {{ \Illuminate\Support\Facades\Auth::check() ? 'for ' . \Illuminate\Support\Facades\Auth::user()->name : ''  }}</h1>
        </div>
    </p>

    <p>
        <div>

        </div>
    </p>

    <p>
        @foreach(\App\Models\Category::get() as $category)
            <a href="{{ route('customer_order', ['categoryId' => $category->id]) }}">{{ $category->name }}</a>
        @endforeach
        <a href="{{ route('customer_order') }}">Все товары</a>
    </p>

    <p>
        <div>
            <form action="{{ route('customer_order_create') }}" method="post">
                @csrf
                @if(\Illuminate\Support\Facades\Auth::check())
                <label>Enter your phone number in format 38-xxx-xxx-xx-xx</label>
                <input type="tel" name="phone" required pattern="[0-9]{2}-[0-9]{3}-[0-9]{2}-[0-9]{3}-[0-9]{2}" value="{{ old('phone') }}">
                @endif
                <select multiple size="3" name="productId" >
                    @foreach($categories as $category)
                        @foreach($category->products as $product)
                            <option value="{{ $product->id }}">
                                {{ $category->name }}: {{ $product->product }}
                            </option>
                        @endforeach
                    @endforeach
                </select>
                @if(\Illuminate\Support\Facades\Auth::check())
                    <button type="submit">Create Order</button>
                @else For purchasing do authentication please!
                @endif
            </form>
        </div>
    @include('errors-check')
    </p>

    @include('errors-check')

     <p>
         <div>
            @if(\Illuminate\Support\Facades\Auth::check())
                <h2>Dear {{ \Illuminate\Support\Facades\Auth::user()->name }}, you have these orders:</h2>
                @if($orders->count() > 0)
                    <table>
                        <tr>
                            <th>Имя пользователя</th>
                            <th>Email</th>
                            <th>Телефон</th>
                            <th>Категория</th>
                            <th>Продукт</th>
                            <th>Дата создания заказа</th>
                            <th>Действие</th>
                        </tr>
                        @foreach($orders as $order)
                            <tr>
                                <td>{{ $order->name }}</td>
                                <td>{{ $order->email }}</td>
                                <td>{{ $order->phone }}</td>
                                <td>{{ $order->products->category->name }}</td>
                                <td>{{ $order->products->product }}</td>
                                <td>{{ $order->created_at->format('Y-m-d H:i:s') }}</td>
                                <td>
                                    <a href="{{ route('customer_order_delete', $order->id) }}">Delete Order</a>
                                </td>
                            </tr>
                        @endforeach
                    </table>
            @else
                <p>Unfortunately, you don't have active orders new</p>
                @endif

                @if($trashedOrders->count() > 0)
                    <p>
                    <div>
                        <h2>
                            Dear {{ \Illuminate\Support\Facades\Auth::user()->name }}, you have trashed orders!
                        </h2>
                        <table>
                            <tr>
                                <th>Имя пользователя</th>
                                <th>Email</th>
                                <th>Телефон</th>
                                <th>Продукт</th>
                                <th>Дата создания заказа</th>
                                <th>Дата удаления заказа</th>
                                <th>Действие</th>
                            </tr>
                            @foreach($trashedOrders as $trashedOrder)
                                <tr>
                                    <td>{{ $trashedOrder->name }}</td>
                                    <td>{{ $trashedOrder->email }}</td>
                                    <td>{{ $trashedOrder->phone }}</td>
                                    <td>{{ $trashedOrder->products->product }}</td>
                                    <td>{{ $trashedOrder->created_at->format('Y-m-d H:i:s') }}</td>
                                    <td>{{ $trashedOrder->deleted_at->format('Y-m-d H:i:s') }}</td>
                                    <td>
                                        <a href="{{ route('customer_order_restore', $trashedOrder->id) }}">Restore Order</a>
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                        <a href="{{ route('customer_order_restore_all') }}">Restore All Order</a>
                    </div>
                    </p>
                @endif

            @endif
        </div>
    </p>


@endsection
